import { fragment } from "./util.js"

export class BaseElement extends HTMLElement {
	initialized = false
	use_shadow_dom = true
	use_id_cache = true

	$ = {}
	fragment = fragment(this.ownerDocument)

	constructor() {
		super()
		if (this.use_shadow_dom) {
			this.attachShadow({ mode: "open" })
		}
	}

	get child_root() {
		return this.shadowRoot || this
	}

	qs(selector) {
		return this.child_root.querySelector(selector)
	}

	qsa(selector) {
		return this.child_root.querySelectorAll(selector)
	}

	on(elements, events, fn, opt) {
		if (Array.isArray(elements)) {
			for (const elem of elements) {
				this.on(elem, event, fn, opt)
			}
			return
		}
		if (Array.isArray(events)) {
			for (const event of events) {
				this.on(elements, event, fn, opt)
			}
			return
		}
		if (typeof elements === "string") {
			for (const elem of this.qsa(elements)) {
				this.on(elem, event, fn, opt)
			}
			return
		}
		const elem = elements
		const event = events
		elem.addEventListener(event, fn, opt)
	}

	connectedCallback() {
		if (!this.initialized) {
			const fr = this.template
			if (fr) {
				this.child_root.append(fr)

				// collect id
				if (this.use_id_cache) {
					this.$ = {}
					for (const elem of this.qsa("[id]")) {
						if (elem.id.match(/^[a-z0-9\-]+$/)) {
							const id = elem.id.replace(/-/g, "_")
							this.$[id] = elem
						}
					}
				}
			}
			this.initialize()
			this.initialized = true
		} else {
			this.reconnectedCallback()
		}
	}

	/// implement @childclass
	reconnectedCallback() {
		// nothing to do
	}

	/// implement @childclass
	initialize() {
		// nothing to do
	}

	/// implement @childclass
	get template() {
		return fragment``
	}

	propagate(ctx) {
		this._ctx = ctx
		this.update(ctx.event)
		const elems = this.getProgationTargets(ctx.event) ?? []
		for (const elem of elems) {
			if (Array.isArray(elem)) {
				this.propagateChild(elem[0], elem[1])
			} else {
				this.propagateChild(elem, ctx.event)
			}
		}
	}

	propagateChild(child, event) {
		child.propagate({ ...this._ctx, event })
	}

	/// returns array of elements or map of element => new event
	getProgationTargets(event) {
		return []
	}

	/// implement @childclass
	update(event) {
		// nothing to do
	}

	request(...args) {
		if (this._ctx) {
			return this._ctx.request(...args)
		} else {
			throw new Error("cannot call request method before connected event")
		}
	}

	getData(...args) {
		if (this._ctx) {
			return this._ctx.getData(...args)
		} else {
			throw new Error("cannot call getData method before connected event")
		}
	}
}
