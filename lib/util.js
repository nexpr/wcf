export const escape_map = {
	"&": "&amp;",
	">": "&gt;",
	"<": "&lt;",
	'"': "&quot;",
}
export const escape_rex = new RegExp("[" + Object.keys(escape_map) + "]", "g")
export const escape = (str) => str.replace(escape_rex, (x) => escape_map[x])

export const html_stringify = (value) => {
	if (typeof value === "string") return escape(value)
	if (Array.isArray(value)) return value.map(html_stringify).join("")
	if (value && value.html) return value.html
	if (value && value.text) return escape(value.text)
	return escape(String(value))
}
export const html = (templates, ...values) =>
	templates.reduce((a, b) => a + html_stringify(values.shift()) + b)

export const fragment = owner_doc => (templates, ...values) => {
	const template = owner_doc.createElement("template")
	template.innerHTML = html(templates, ...values)
	return template.content.cloneNode(true)
}
