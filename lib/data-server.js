export const createDataServer = (def, initial) => {
	const fns = new Set()
	const data = initial

	const request = (type, ...values) => {
		if (typeof def[type] === "function") {
			const event = def[type]({ getData, send, type}, ...values)
			if (event) send(event)
		} else {
			throw new Error("unknown type: " + type)
		}
	}

	const send = (event) => {
		for (const fn of fns) {
			fn({ event, request, getData })
		}
	}

	const listen = (fn) => {
		if (typeof fn !== "function" && fn.propagate) {
			const view = fn
			fn = (ctx) => view.propagate(ctx)
		}
		fns.add(fn)
		send({ type: "connected" })
	}

	const getData = () => {
		return data
	}

	return { request, listen }
}
