import "./clock-element.js"
import { ds } from "./ds.js"

const clockelem = document.createElement("clock-element")
document.getElementById("root").append(clockelem)

ds.listen(clockelem)

setInterval(() => {
	ds.request("time", new Date())
}, 1000)
