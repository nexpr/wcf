import { BaseElement } from "../lib/base-element.js"

customElements.define(
	"number-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<span id="num"></span>
			`
		}

		update({ type, number, digit }) {
			if (type === "change") {
				const num = +number || 0
				const di = +digit || 0
				this.$.num.textContent = String(+num).padStart(di, "0")
			}
		}
	}
)
