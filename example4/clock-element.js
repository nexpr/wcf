import { BaseElement } from "../lib/base-element.js"
import "./number-element.js"

customElements.define(
	"clock-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<number-element id="hour"></number-element>
				:
				<number-element id="minute"></number-element>
				:
				<number-element id="second"></number-element>
			`
		}

		getProgationTargets(event) {
			const d = this.getData().time
			return new Map([
				[this.$.hour, { type: "change", number: d.getHours(), digit: 2 }],
				[this.$.minute, { type: "change", number: d.getMinutes(), digit: 2 }],
				[this.$.second, { type: "change", number: d.getSeconds(), digit: 2 }],
			])
		}
	}
)
