import { BaseElement } from "../lib/base-element.js"

customElements.define(
	"count-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<span id="count"></span>
				<input id="plus" type="button" value="+">
			`
		}

		initialize() {
			this.on(this.$.plus, "click", (eve) => {
				this.request("up", 1)
			})
		}

		update({ type }) {
			if (type === "connected" || type === "up") {
				this.$.count.textContent = this.getData().count
			}
		}
	}
)
