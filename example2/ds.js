import { createDataServer } from "../lib/data-server.js"

export const ds = createDataServer(
	{
		up({ getData, type }, value) {
			const data = getData()
			data.count += value
			return { type }
		},
	},
	{
		count: 0,
	}
)
