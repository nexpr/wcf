import { BaseElement } from "../lib/base-element.js"

customElements.define(
	"clock-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<div id="now"></div>
			`
		}

		initialize() {
			this.$.now.textContent = new Date().toLocaleTimeString()
		}

		update({ type, time }) {
			if (type === "time") {
				this.$.now.textContent = new Date(time).toLocaleTimeString()
			}
		}
	}
)
