import { createDataServer } from "../lib/data-server.js"

export const ds = createDataServer({
	time({ type }, time) {
		return { type, time }
	},
})
