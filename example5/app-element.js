import { BaseElement } from "../lib/base-element.js"
import "./number-input.js"

customElements.define(
	"app-element",
	class extends BaseElement {
		elems = []

		get template() {
			return this.fragment`
				<div>
					<select id="num">
						<option>1</option>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
					<div id="numinputs"></div>
					<div>
						Total:
						<span id="total"></span>
					</div>
				</div>
			`
		}

		initialize() {
			this.on(this.$.num, "change", (eve) => {
				this.request("num-length-change", +eve.target.value)
			})
		}

		update({ type }) {
			const data = this.getData()
			if (type === "connected" || type === "num-length-change") {
				const len = data.numbers.length
				this.$.num.value = len
				while (len > this.elems.length) {
					const n = document.createElement("number-input")
					const index = this.elems.length
					this.elems.push(n)
					this.$.numinputs.append(n)
					this.propagateChild(n, {
						type: "connected",
						onUp: () => this.request("up", index),
						onDown: () => this.request("down", index),
						value: data.numbers[index],
					})
				}
				while (len < this.elems.length) {
					const elem = this.elems.pop()
					elem.remove()
				}
				this.$.total.textContent = data.numbers.reduce((a, b) => a + b)
			}
		}

		getProgationTargets(event) {
			if (event.type === "num-value-change") {
				const elem = this.elems[event.index]
				const num = this.getData().numbers[event.index]
				return new Map([[elem, { type: "updated", value: num }]])
			}
		}
	}
)
