import { BaseElement } from "../lib/base-element.js"

customElements.define(
	"number-input",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<div>
					<input id="text" type="text" value="0" readonly>
					<input id="up" type="button" value="+">
					<input id="down" type="button" value="-">
				</div>
			`
		}

		initialize() {
			this.on(this.$.up, "click", (eve) => {
				this.__onUp()
			})
			this.on(this.$.down, "click", (eve) => {
				this.__onDown()
			})
		}

		update(event) {
			if (event.type === "connected") {
				this.__onUp = event.onUp
				this.__onDown = event.onDown
				this.$.text.value = event.value
			}
			if (event.type === "updated") {
				this.$.text.value = event.value
			}
		}
	}
)
