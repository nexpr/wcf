import { createDataServer } from "../lib/data-server.js"

export const ds = createDataServer(
	{
		"num-length-change"({getData}, num) {
			const data = getData()
			if (data.numbers.length > num) {
				data.numbers.length = num
			} else {
				while (data.numbers.length < num) {
					data.numbers.push(0)
				}
			}
			return { type: "num-length-change" }
		},
		up({ getData, type }, index) {
			const data = getData()
			data.numbers[index] += 1
			return { type: "num-value-change", index }
		},
		down({ getData, type }, index) {
			const data = getData()
			data.numbers[index] -= 1
			return { type: "num-value-change", index }
		},
	},
	{
		numbers: [0, 0],
	}
)
