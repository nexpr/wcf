import "./app-element.js"
import { ds } from "./ds.js"

const appelem = document.createElement("app-element")
document.getElementById("root").append(appelem)

ds.listen(appelem)

