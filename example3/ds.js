import { createDataServer } from "../lib/data-server.js"

export const ds = createDataServer(
	{
		time({ getData, type }, time) {
			const data = getData()
			data.time = time
			return { type }
		},
	},
	{
		time: new Date(),
	}
)
