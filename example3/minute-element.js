import { BaseElement } from "../lib/base-element.js"

customElements.define(
	"minute-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<span id="minute"></span>
			`
		}

		update({ type }) {
			if (type === "connected" || type === "time") {
				const time = this.getData().time
				this.$.minute.textContent = time.getMinutes().toString().padStart(2, "0")
			}
		}
	}
)
