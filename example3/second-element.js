import { BaseElement } from "../lib/base-element.js"

customElements.define(
	"second-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<span id="second"></span>
			`
		}

		update({ type }) {
			if (type === "connected" || type === "time") {
				const time = this.getData().time
				this.$.second.textContent = time.getSeconds().toString().padStart(2, "0")
			}
		}
	}
)
