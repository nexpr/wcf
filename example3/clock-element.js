import { BaseElement } from "../lib/base-element.js"
import "./hour-element.js"
import "./minute-element.js"
import "./second-element.js"

customElements.define(
	"clock-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<hour-element id="hour"></hour-element>
				:
				<minute-element id="minute"></minute-element>
				:
				<second-element id="second"></second-element>
			`
		}

		getProgationTargets(event) {
			return [this.$.hour, this.$.minute, this.$.second]
		}
	}
)
