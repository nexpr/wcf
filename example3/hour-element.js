import { BaseElement } from "../lib/base-element.js"

customElements.define(
	"hour-element",
	class extends BaseElement {
		get template() {
			return this.fragment`
				<span id="hour"></span>
			`
		}

		update({ type }) {
			if (type === "connected" || type === "time") {
				const time = this.getData().time
				this.$.hour.textContent = time.getHours().toString().padStart(2, "0")
			}
		}
	}
)
